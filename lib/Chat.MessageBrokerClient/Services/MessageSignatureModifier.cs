﻿using Chat.MessageBrokerClient.Configurations;
using Chat.MessageBrokerClient.Models;
using Chat.MessageBrokerClient.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Services
{
    public class MessageSignatureModifier : IMessageSignatureModifier
    {
        private readonly ClientConfiguration _clientConfiguration;

        public MessageSignatureModifier(ClientConfiguration clientConfiguration)
        {
            _clientConfiguration = clientConfiguration;
        }

        public MessageSignature ModifySignature(MessageSignature signature)
        {
            signature.Signature = _clientConfiguration.InstanceSignature;
            signature.GroupId = _clientConfiguration.GroupId;

            return signature;
        }
    }
}
