﻿using Chat.MessageBrokerClient.Configurations;
using Chat.MessageBrokerClient.Models;
using Chat.MessageBrokerClient.Services.Interfaces;
using Confluent.Kafka;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chat.MessageBrokerClient.Services
{
    public class KafkaMessageBrokerClient : IMessageBrokerClient
    {
        private readonly ReaderWriterLockSlim _mutex;
        private readonly ServerConfiguration _serverConfiguration;
        private readonly ClientConfiguration _clientConfiguration;
        private readonly ProducerConfig _producerConfig;
        private readonly MessageSignatureModifier _signatureModifier;
        private IProducer<Null, byte[]> _producer;
        private long startingOffset = -1001;

        public KafkaMessageBrokerClient(IOptions<ServerConfiguration> serverConfiguration, IOptions<ClientConfiguration> clientConfiguration)
        {
            _mutex = new ReaderWriterLockSlim();

            _serverConfiguration = serverConfiguration.Value;
            _clientConfiguration = clientConfiguration.Value;
            _producerConfig = new ProducerConfig
            {
                BootstrapServers = serverConfiguration.Value.BrokerList,
                Acks = Acks.Leader,
                EnableDeliveryReports = false
            };

            _signatureModifier = new MessageSignatureModifier(clientConfiguration.Value);
            CreateProducer();
        }

        public void Dispose()
        {

        }

        public void SetStartingOffset(long offset)
        {
            Console.WriteLine($"Setting starting offset to: {offset}");
            this.startingOffset = offset;
        }

        public async Task<T> Send<T>(T message, string topicName) where T : IMessage
        {
            message.Signature = _signatureModifier.ModifySignature(message.Signature);

            var ms = new MemoryStream();
            using (var writer = new BsonDataWriter(ms))
            {
                var serializer = new JsonSerializer();
                serializer.DefaultValueHandling = DefaultValueHandling.Ignore;
                serializer.NullValueHandling = NullValueHandling.Ignore;
                serializer.TypeNameHandling = TypeNameHandling.Objects;
                serializer.Serialize(writer, message);
            }

            var m = new Message<Null, byte[]>
            {
                Value = ms.ToArray()
            };

            var report = await _producer.ProduceAsync(topicName, m);
            message.Offset = report.Offset;
            return message;
        }

        public IObservable<T> Consume<T>() where T : IMessage
        {
            var topicName = typeof(T).ToString();
            Action<IConsumer<Ignore, byte[]>> consumerAssignFunc = consumer =>
            {
                if (startingOffset != Offset.Unset.Value)
                {
                    Console.WriteLine($"Assigning to a partition starting from offset {startingOffset}");
                    consumer.Assign(new TopicPartitionOffset(topicName, 0, new Offset(startingOffset)));
                }
                else
                {
                    consumer.Subscribe(topicName);
                }
            };


            return DoConsume<T>(consumerAssignFunc, msg => msg.Topic == topicName);
        }

        private IProducer<Null, byte[]> CreateProducer()
        {
            try
            {
                _mutex.EnterWriteLock();
                if (_producer != null)
                {
                    try
                    {
                        _producer.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Console.Write($"Producer dispose error: {ex}");
                    }
                }

                var producerBuilder = new ProducerBuilder<Null, byte[]>(_producerConfig);
                _producer = producerBuilder.Build();

                producerBuilder.SetErrorHandler((s, e) =>
                {
                    CreateProducer();
                });
            }
            finally
            {
                _mutex.ExitWriteLock();
            }

            return _producer;
        }

        private IConsumer<Ignore, T> CreateConsumer<T>(bool signal)
        {
            var config = new ConsumerConfig()
            {
                BootstrapServers = _serverConfiguration.BrokerList,
                GroupId = signal ? _clientConfiguration.GroupId : Guid.NewGuid().ToString(),
                EnableAutoCommit = true,
                StatisticsIntervalMs = 60000,
            };

            config.Set("queue.buffering.max.ms", "5");
            config.Set("batch.num.messages", "5");
            config.Set("socket.blocking.max.ms", "5");

            var consumerBuilder = new ConsumerBuilder<Ignore, T>(config);

            var consumer = consumerBuilder.Build();

            consumerBuilder.SetErrorHandler((c, error) =>
            {
                
            });

            //consumer.OnError += (obj, error) =>
            //{
            //    Console.WriteLine($"Consumer Error: {error.Code}");
            //};

            //consumer.OnPartitionEOF += (obj, end) =>
            //{
            //    Console.WriteLine($"Reached end of topic {end.Topic} partition {end.Partition}, next message will be at offset {end.Offset}");
            //};

            //consumer.OnPartitionsAssigned += (obj, partitions) =>
            //{
            //    Console.WriteLine($"Assigned partitions: [{string.Join(", ", partitions)}], member id: {consumer.MemberId}");
            //    consumer.Assign(partitions);
            //};

            //consumer.OnPartitionsRevoked += (obj, partitions) =>
            //{
            //    // Wpis pojawia się wtedy, gdy mamay uruchomione 2 usługi z tym samym groupId i są zapisane na ten sam topic to wtedy tylko jedna jest aktywna - działają jako active-passive
            //    Console.WriteLine($"Revoked partitions: [{string.Join(", ", partitions)}]");
            //    consumer.Unassign();
            //};

            return consumer;
        }

        private IObservable<T> DoConsume<T>(Action<IConsumer<Ignore, byte[]>> consumerAssignAction,
            Func<ConsumeResult<Ignore, byte[]>, bool> filterFunc) where T : IMessage
        {
            return Observable.Create<T>(observer =>
            {
                var subscription = new CompositeDisposable();
                bool consuming = true;
                var subj = new Subject<T>();
                subscription.Add(subj.Subscribe(observer));

                var cancelTokenSource = new CancellationTokenSource();

                var consumer = CreateConsumer<byte[]>(true);
                consumerAssignAction(consumer);

                
                //consumer.OnError += (sender, e) => consuming = !e.IsFatal;
                Task.Factory.StartNew(() => Observable.Generate((ConsumeResult<Ignore, byte[]>)null,
                        _ => consuming && !cancelTokenSource.IsCancellationRequested,
                        _ =>
                        {
                            try
                            {
                                var cr = consumer.Consume(TimeSpan.FromSeconds(1));
                                return cr;
                            }
                            catch (ConsumeException ex)
                            {
                                Console.WriteLine($"Consumer Error: {ex.Error.Code}");
                                return null;
                            }
                        },
                        msg => msg)
                    .Where(msg => msg != null && filterFunc(msg))
                    .Select(msg =>
                    {
                        MemoryStream ms = new MemoryStream(msg.Value);
                        using (var reader = new BsonDataReader(ms))
                        {
                            var serializer = new JsonSerializer();

                            serializer.DefaultValueHandling = DefaultValueHandling.Ignore;
                            serializer.NullValueHandling = NullValueHandling.Ignore;
                            serializer.TypeNameHandling = TypeNameHandling.Objects;
                            try
                            {
                                var message = serializer.Deserialize<T>(reader);
                                message.Offset = msg.Offset;
                                Console.WriteLine($"Received message from topic: [{msg.Topic}] type: [{message.GetType()}]");
                                return message;
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Deserialize error: {ex}");
                                return default(T);
                            }
                        }
                    })
                    .Subscribe(@event => subj.OnNext(@event),
                            () =>
                            {
                                Console.WriteLine($"Completed consumer");

                                consumer.Dispose();
                            }),
                    cancelTokenSource.Token, TaskCreationOptions.LongRunning,
                    TaskScheduler.Default);
                subscription.Add(Disposable.Create(() => cancelTokenSource.Cancel()));
                return subscription;
            })
                .Catch<T, Exception>(ex =>
                {
                    Console.WriteLine(ex);
                    throw ex;
                })
                .Retry();
        }
    }
}
