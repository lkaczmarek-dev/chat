﻿using Chat.MessageBrokerClient.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Services.Interfaces
{
    public interface IMessageSignatureModifier
    {
        MessageSignature ModifySignature(MessageSignature signature);
    }
}
