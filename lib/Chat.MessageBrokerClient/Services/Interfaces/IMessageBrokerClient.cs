﻿using Chat.MessageBrokerClient.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Chat.MessageBrokerClient.Services.Interfaces
{
    public interface IMessageBrokerClient : IDisposable
    {
        IObservable<T> Consume<T>() where T : IMessage;
        Task<T> Send<T>(T message, string topicName) where T : IMessage;
        void SetStartingOffset(long offset);
    }
}
