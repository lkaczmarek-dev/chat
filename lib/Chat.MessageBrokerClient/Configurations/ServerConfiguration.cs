﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Configurations
{
    public class ServerConfiguration
    {
        public string BrokerList { get; set; }
    }
}
