﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Configurations
{
    public class ClientConfiguration
    {
        public string GroupId { get; set; }
        public Guid InstanceSignature { get; set; }
    }
}
