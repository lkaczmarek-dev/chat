﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Models
{
    public class MessageSignature
    {
        public Guid Id { get; set; }
        public string GroupId { get; set; }
        public Guid Signature { get; set; }
        public DateTime GeneratedDate { get; set; }
    }
}
