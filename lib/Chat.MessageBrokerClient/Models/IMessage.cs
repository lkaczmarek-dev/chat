﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.MessageBrokerClient.Models
{
    public interface IMessage
    {
        long Offset { get; set; }
        MessageSignature Signature { get; set; }
    }
}
