﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Architecture.Events
{
    public class UserJoinedEvent : EventBase
    {
        public string Name { get; set; }
    }
}
