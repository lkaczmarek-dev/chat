﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Architecture.Events
{
    public class UsersCountChangedEvent : EventBase
    {
        public long UsersCount { get; set; }
    }
}
