﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Architecture.Events
{
    public class ChatMessageEvent : EventBase
    {
        public string Sender { get; set; }

        public string Text { get; set; }
    }
}
