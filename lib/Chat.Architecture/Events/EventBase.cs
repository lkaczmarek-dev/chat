﻿using Chat.MessageBrokerClient.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Architecture.Events
{
    public abstract class EventBase : IMessage
    {
        public DateTime Date { get; set; }
        public long Offset { get; set; }
        public MessageSignature Signature { get; set; }

        public EventBase()
        {
            Signature = new MessageSignature();
        }
    }
}
