﻿using Blazored.SessionStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.WebApp.Services
{
    public class UserService
    {
        private readonly ISessionStorageService _sessionStorageService;
        private const string CURRENT_USER_KEY = "currentUser";

        public string UserName { get; private set; }
        public bool IsLogged { get; private set; }

        public UserService(ISessionStorageService sessionStorageService)
        {
            _sessionStorageService = sessionStorageService;
        }

        public async Task Initialize()
        {
            await RestoreLoginData();
        }

        public async Task Login(string userName)
        {
            await _sessionStorageService.SetItemAsync(CURRENT_USER_KEY, userName);

            await RestoreLoginData();
        }

        public async Task Logout()
        {
            await _sessionStorageService.RemoveItemAsync(CURRENT_USER_KEY);

            await RestoreLoginData();
        }

        private async Task RestoreLoginData()
        {
            var userName = await _sessionStorageService.GetItemAsync<string>(CURRENT_USER_KEY);
            if (!string.IsNullOrEmpty(userName))
            {
                UserName = userName;
                IsLogged = true;
            }
            else
            {
                UserName = string.Empty;
                IsLogged = false;
            }
        }
    }
}
