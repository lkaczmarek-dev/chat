﻿using Chat.Architecture.Events;
using Chat.HubApp.Services.Interfaces;
using Chat.MessageBrokerClient.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Services
{
    public class EventPublisher : IEventPublisher
    {
        private readonly IMessageBrokerClient _messageBrokerClient;

        public EventPublisher(IMessageBrokerClient messageBrokerClient)
        {
            _messageBrokerClient = messageBrokerClient;
        }

        public async Task Publish(EventBase @event)
        {
            await _messageBrokerClient.Send(@event, typeof(EventBase).FullName);
        }
    }
}
