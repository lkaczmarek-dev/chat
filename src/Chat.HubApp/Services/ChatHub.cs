﻿using Chat.Architecture.Events;
using Chat.HubApp.Repositories.Interfaces;
using Chat.HubApp.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace Chat.HubApp.Services
{
    public class ChatHub : Hub
    {
        private readonly IEventPublisher _eventPublisher;
        private readonly IEventStore _eventStore;
        private readonly ILocalSubscriptionsRepository _subscriptionsRepository;
        private readonly IPersistentRepository _usersRepository;

        public ChatHub(IEventPublisher eventPublisher, IEventStore eventStore, ILocalSubscriptionsRepository subscriptionsRepository, IPersistentRepository usersRepository)
        {
            _eventPublisher = eventPublisher;
            _eventStore = eventStore;
            _subscriptionsRepository = subscriptionsRepository;
            _usersRepository = usersRepository;
        }

        public async Task SendMessage(string text)
        {
            var sender = Context.GetHttpContext().Request.Headers["Username"];

            var chatMessage = new ChatMessageEvent()
            {
                Date = DateTime.Now,
                Sender = sender,
                Text = text
            };

            await _eventPublisher.Publish(chatMessage);
        }

        public override async Task OnConnectedAsync()
        {
            var sender = Context.GetHttpContext().Request.Headers["Username"];

            SubscribeUserForEvents();

            var userJoinedEvent = new UserJoinedEvent()
            {
                Date = DateTime.Now,
                Name = sender
            };

            await _eventPublisher.Publish(userJoinedEvent);

            await _usersRepository.Add(Context.ConnectionId);
            await SendUsersCountChangedEvent();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var sender = Context.GetHttpContext().Request.Headers["Username"];

            _subscriptionsRepository.Remove(Context.ConnectionId);
            await _usersRepository.Remove(Context.ConnectionId);

            var userLeftEvent = new UserLeftEvent()
            {
                Date = DateTime.Now,
                Name = sender
            };

            await _eventPublisher.Publish(userLeftEvent);
            await SendUsersCountChangedEvent();
        }

        private void SubscribeUserForEvents()
        {
            var caller = Clients.Caller;

            // at the moment it could be merged to one SignalR method like "SendEvent" but it would require more job on frontend site
            //_eventStore.Events.Subscribe(async @event => await caller.SendAsync("SendEvent", @event));

            var disposables = new CompositeDisposable();
            disposables.Add(_eventStore.Events.OfType<ChatMessageEvent>().Subscribe(async @event =>
            {
                await caller.SendAsync("Message", @event);
            }));

            disposables.Add(_eventStore.Events.OfType<UserJoinedEvent>().Subscribe(async @event =>
            {
                await caller.SendAsync("UserJoined", @event);
            }));

            disposables.Add(_eventStore.Events.OfType<UserLeftEvent>().Subscribe(async @event =>
            {
                await caller.SendAsync("UserLeft", @event);
            }));

            disposables.Add(_eventStore.Events.OfType<UsersCountChangedEvent>().Subscribe(async @event =>
            {
                await caller.SendAsync("UsersCountChanged", @event);
            }));

            _subscriptionsRepository.Add(Context.ConnectionId, disposables);
        }

        private async Task SendUsersCountChangedEvent()
        {
            var usersCount = await _usersRepository.GetLoggedUsersCount();
            var usersCountChangedEvent = new UsersCountChangedEvent()
            {
                Date = DateTime.Now,
                UsersCount = usersCount
            };

            await _eventPublisher.Publish(usersCountChangedEvent);
        }
    }
}
