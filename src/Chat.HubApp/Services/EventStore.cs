﻿using Chat.Architecture.Events;
using Chat.HubApp.Services.Interfaces;
using Chat.MessageBrokerClient.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace Chat.HubApp.Services
{
    public class EventStore : IEventStore
    {
        private readonly ReplaySubject<EventBase> _events = new ReplaySubject<EventBase>();

        public IObservable<EventBase> Events => _events;

        public EventStore(IMessageBrokerClient messageBrokerClient)
        {
            messageBrokerClient.SetStartingOffset(0);

            messageBrokerClient.Consume<EventBase>()
                .Subscribe(e => _events.OnNext(e));
        }
    }
}
