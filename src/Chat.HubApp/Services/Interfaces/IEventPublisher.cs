﻿using Chat.Architecture.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Services.Interfaces
{
    public interface IEventPublisher
    {
        Task Publish(EventBase @event);
    }

}
