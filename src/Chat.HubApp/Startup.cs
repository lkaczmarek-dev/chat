using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.HubApp.Configurations;
using Chat.HubApp.Repositories;
using Chat.HubApp.Repositories.Interfaces;
using Chat.HubApp.Services;
using Chat.HubApp.Services.Interfaces;
using Chat.MessageBrokerClient.Configurations;
using Chat.MessageBrokerClient.Services.Interfaces;
using Chat.MessageBrokerClient.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.Reactive.Linq;
using Chat.Architecture.Events;
using System.Security.Cryptography;

namespace Chat.HubApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ServerConfiguration>(Configuration.GetSection("MessageBrokerServerConfiguration"));
            services.Configure<ClientConfiguration>(Configuration.GetSection("MessageBrokerClientConfiguration"));
            services.Configure<RedisConfiguration>(Configuration.GetSection("RedisConfiguration"));

            services.AddTransient<IMessageBrokerClient, KafkaMessageBrokerClient>();

            services.AddSignalR();
            services.AddSingleton<IEventStore, EventStore>();
            services.AddTransient<IEventPublisher, EventPublisher>();
            services.AddSingleton<ILocalSubscriptionsRepository, LocalSubscriptionsRepository>();
            services.AddTransient<IPersistentRepository, PersistentRedisRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ChatHub>("/chathub");
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
