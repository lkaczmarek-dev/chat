﻿using Chat.HubApp.Repositories.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Repositories
{
    public class LocalSubscriptionsRepository : ILocalSubscriptionsRepository
    {
        private readonly ConcurrentDictionary<string, IDisposable> _subscriptions = new ConcurrentDictionary<string, IDisposable>();

        public void Add(string connectionId, IDisposable subscription)
        {
            _subscriptions.AddOrUpdate(connectionId, subscription, (key, previousSubscription) => subscription);
        }

        public void Remove(string connectionId)
        {
            if (_subscriptions.Remove(connectionId, out var subscription))
            {
                subscription?.Dispose();
            }
        }
    }
}
