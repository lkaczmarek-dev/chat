﻿using Chat.HubApp.Configurations;
using Chat.HubApp.Repositories.Interfaces;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Repositories
{
    /// <summary>
    /// Repository which store subscribers information from all instances of HubApp based on Redis
    /// </summary>
    public class PersistentRedisRepository : IPersistentRepository
    {
        private const string LOGGED_USERS_SET_NAME = "users";
        private readonly IOptions<RedisConfiguration> _redisConfiguration;

        public PersistentRedisRepository(IOptions<RedisConfiguration> redisConfiguration)
        {
            _redisConfiguration = redisConfiguration;
        }

        public async Task Add(string connectionId)
        {
            // TODO - Keep connection opened, handle reconnect
            var redis = ConnectionMultiplexer.Connect(_redisConfiguration.Value.RedisAddress);
            var db = redis.GetDatabase();

            await db.SetAddAsync(LOGGED_USERS_SET_NAME, connectionId);
        }

        public async Task Remove(string connectionId)
        {
            var redis = ConnectionMultiplexer.Connect(_redisConfiguration.Value.RedisAddress);
            var db = redis.GetDatabase();

            await db.SetRemoveAsync(LOGGED_USERS_SET_NAME, connectionId);
        }

        public async Task<long> GetLoggedUsersCount()
        {
            var redis = ConnectionMultiplexer.Connect(_redisConfiguration.Value.RedisAddress);
            var db = redis.GetDatabase();

            return await db.SetLengthAsync(LOGGED_USERS_SET_NAME);
        }
    }
}
