﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Repositories.Interfaces
{
    /// <summary>
    /// Repository which store subscribers information from all instances of HubApp
    /// </summary>
    public interface IPersistentRepository
    {
        Task Add(string connectionId);

        Task Remove(string connectionId);

        Task<long> GetLoggedUsersCount();
    }
}
