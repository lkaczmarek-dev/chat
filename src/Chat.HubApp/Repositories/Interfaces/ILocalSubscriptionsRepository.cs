﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Repositories.Interfaces
{
    public interface ILocalSubscriptionsRepository
    {
        void Add(string connectionId, IDisposable subscription);

        void Remove(string connectionId);
    }
}
