﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat.HubApp.Configurations
{
    public class RedisConfiguration
    {
        public string RedisAddress { get; set; }
    }
}
